<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
        'user_id',
        'currency',
        'start_date',
        'end_date',
        'trip_length',
        'total_quote',
        'ages'
    ];

    protected $casts = [
        'ages' =>'array'
    ];



}
