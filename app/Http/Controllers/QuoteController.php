<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AgeLoad;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Quote;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class QuoteController extends Controller
{

  public function __construct() {
    $this->middleware('auth');

  }

  public function generateQuote(Request $request) {
    $userId = Auth::id();
    $currency = $request->input('currency');
    $startDate = Carbon::parse($request->input('startDate'));
    $endDate = Carbon::parse($request->input('endDate'));
    $ages = $request->input('ages');
    $total = 0;
    $loads = [];

    //getTripLength
    $tripLength = $startDate->diffInDays($endDate->addDay());
    
    if(in_array(0, $ages)) {
      $response['status'] = 0;
      $response['code'] = 400;
      $response['data'] = null;
      $response['message'] = '0 is not a valid age';
    } 
    else if($ages[0] <=17) {
      $response['status'] = 2;
      $response['code'] = 400;
      $response['data'] = null;
      $response['message'] = 'First age must be 18 or older';
    } else {
        foreach ($ages as $age) {
          $load = $this->getAgeLoad($age);
          array_push($loads, $load);
          $total += (3 * $load * $tripLength);
        }
        $formattedTotal = number_format($total, 2);
        
        $quote = Quote::create([
          'user_id' => $userId,
          'currency' => $request->currency,
          'start_date' => $request->startDate,
          'end_date' => $request->endDate,
          'trip_length' => $tripLength,
          'total_quote' => $formattedTotal,
          'ages' => $ages
        ]);
        $quote->save();
        $quoteId = $quote->id;
        
        $data = [
          'quote_total' => $formattedTotal,
          'currency' => $currency,
          'quote_id' => $quoteId
        ];
        $response['status'] = 1;
        $response['code'] = 200;
        $response['data'] = $data;
        $response['message'] = 'Success';
      }

    return $response;

  }

  protected function getAgeLoad($age) {
    $load = 0;

    if($age >= 18 && $age <=30) {
      $load = 0.6;
    }
    if($age >= 31 && $age <=40) {
      $load = 0.7;
    }
    if($age >= 41 && $age <=50) {
      $load = 0.8;
    }
    if($age >= 51 && $age <=60) {
      $load = 0.9;
    }
    if($age >= 61 && $age <=70) {
      $load = 1.0;
    }
    return $load;
  }

  // protected function guard() {
  //   return Auth::guard();
  // }
  

  
}
