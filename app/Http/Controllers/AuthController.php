<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }


   public function register(Request $request) {
    $user = User::where('email', $request['email'])->first();

        if($user) {
            $response['status'] = 0;
            $response['message'] = 'Email Already Exists';
            $response['code'] = 409;
        } else if ($request === null) {
            $response['status'] = 2;
            $response['message'] = 'All fields are required';
            $response['code'] = 401;
        }
        else {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
            $response['status'] = 1;
            $response['message'] = 'User Registered Successfully';
            $response['code'] = 200;
        }
        return $response;
   }

   public function login(Request $request) {
       $credentials = $request->only('email', 'password');

       try {
            if(!JWTAuth::attempt($credentials)) {
            $response['status'] = 0;
            $response['code'] = 401;
            $response['data'] = null;
            $response['message'] = 'Email or Password is incorrect';
            return $response;
            }
       } catch(JWTException $e) {
            $response['data'] = null;
            $response['code'] = 500;
            $response['message'] = 'Could not create token';
            return $response;
       }
       $user = Auth::user();
    //    $user = auth()->user();
       $data['token'] = Auth::claims([
           'user_id' => $user->id,
           'email' => $user->email,
       ])->attempt($credentials);
            $response['data'] = $data;
            $response['status'] = 1;
            $response['code'] = 200;
            $response['message'] = 'Login Successfully';

        return $response;

   }

}
