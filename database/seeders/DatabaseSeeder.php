<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AgeLoad;
use App\Models\User;
use App\Models\Quote;
use App\Models\QuoteAge;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // QuoteAge::truncate();
        // Quote::truncate();
        // User::truncate();
        // AgeLoad::truncate();
        // \App\Models\User::factory(10)->create();

        $user = User::create([
            'name' => 'Jim Nasium',
            'email' => 'jim@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC' // password
        ]);


        $quote = Quote::create([
            'user_id' => $user->id,
            'currency' => 'USD',
            'start_date' => '2021-12-10',
            'end_date' => '2021-12-20',
            'trip_length' => 11,
            'total_quote' => 117,
            'ages' => [28, 30]
        ]);

    }
}
